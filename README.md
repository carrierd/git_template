# Project Name

Mandatory project description

# Versions

## 2020-07-20 @ 17h40

- {+ R�f�rence [Wrike](http://www.wrike.com) +}
- {+ Ajout du contenu de base dans le MD +}
- {+ Ajout du default [.gitignore](.gitignore) +}

## 2020-07-20 @ 13h30

- [+ Setup du template de GIT +]
- [+ Utilisation des documents par d�fauts +]

# R�f�rence de *Markdown*

Pour des exemples de documentation et le langage *Markdown*, voir [le lien suivant](https://docs.gitlab.com/ee/user/markdown.html#standard-markdown-and-extensions-in-gitlab) ou cet [exemple "live"](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md).

1. Item num�ro 1
   Paragraphe 2 de l'item 1
2. Item num�ro 2
   - Sous-item
   - Again

